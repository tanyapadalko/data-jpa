package com.example.datajpa.controller;

import com.example.datajpa.model.Customer;
import com.example.datajpa.repository.CustomerRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    private CustomerRepository customerRepository;

    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }


    @GetMapping
    public ResponseEntity<List<Customer>> getAllCustomersByLastName(@RequestParam(name = "lastname") String lastname) {
        List<Customer> customers = customerRepository.findByLastName(lastname);
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }

}
